# imgsdemo

#### 介绍
xadmin多图上传

#### 软件架构
只要提供了xadmin后台多图上传功能，本demo是通过重写imagefield实现。


#### 安装教程

1.  python3.5.2
2.  django2.2
3.  xadmin2.0

#### 使用说明

1.  相关库请参照requirments.txt，否则出现库版本问题，请自行解决。
2.  media下的img文件夹必须先建立，否则报文件错误。


#### 特技

