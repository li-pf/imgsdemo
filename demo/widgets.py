from django import forms
from django.utils.safestring import mark_safe

from .models import Img as img#存储图片的表,修改as前的模型函数，保留 as img

class UploaderWidget(forms.FileInput):
    """
    A ImageField Widget that shows its current value if it has one.
    """
    def __init__(self, attrs={}):
        super(UploaderWidget, self).__init__(attrs)

    def render(self, name, value, attrs=None, renderer=None, img =img):
        output = []

        if value and hasattr(value, "url"):
            #多图上传预览
            print(value)
            img_names = img.objects.filter(model=value.instance.pk).values('image')
            print(img_names)
            if img_names:
                for img in img_names:
                    output.append(
                        '<a href="/media/%s" target="_blank" title="多图上传" data-gallery="gallery"> '
                        '<img src="/media/%s" class="field_img">'
                        '</a>' % (img["image"], img["image"]))
        output.append(super(UploaderWidget, self).render(name, value, attrs, renderer))
        """
        因为前端页面中选择按钮变多选,需要在对应标签里添加上multiple="multiple"
        而这里的output就是xadmin后端生成的html所需要的标签的文本流
        所以我们只需要在output的对应位置用代码添加上这一条件
        """
        #多图选择
        if len(output) >= 1:
            output[-1] = output[-1][:-1] + ' multiple="multiple">'
        else:
            output[0] = output[0][:-2] + ' multiple="multiple">'

        return mark_safe(u''.join(output))