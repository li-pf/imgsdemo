from django.db import models
from .fields import UploaderImageField

# Create your models here.
class Uploadimgs(models.Model):
    name = models.CharField(max_length=50,verbose_name='名字')
    imgs = UploaderImageField(max_length=500,verbose_name='多图上传',null=True, blank=True,upload_to='img')
    class Meta:
        verbose_name = u'多图上传'
        verbose_name_plural = verbose_name


class Img(models.Model):
    model = models.ForeignKey(Uploadimgs,on_delete=models.CASCADE,verbose_name='外键关联')
    creattime = models.DateTimeField(verbose_name='创建时间',auto_now_add=True)
    image = models.ImageField(upload_to='img', verbose_name='图片', null=True, blank=True)

    class Meta:
        verbose_name = u'单图列表'
        verbose_name_plural = verbose_name
