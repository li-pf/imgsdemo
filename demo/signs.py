# -*- coding: utf-8 -*-
# @File    : signs.py
# @Date    : 2021-11-10
# @Author  : admin
# -*- coding: utf-8 -*-
# @File    : signals.py
# @Date    : 2021-11-04
# @Author  : admin
#压缩
from PIL import Image
from io import BytesIO
from django.core.files import File
#引入信号
from django.db.models.signals import pre_save,pre_delete,post_init,post_save
from django.dispatch import receiver
# pre_init                     # model执行构造方法前，触发
# post_init                    # model执行构造方法后，触发
# pre_save                     # model执行save对象保存前，触发
# post_save                    # model执行save对象保存前，触发
# pre_delete                   # model执行delete对象删除前，触发
# post_delete                  # model执行delete对象删除前，触发
# m2m_changed                  # model使用多对多字段操作第三张表前后，触发
# class_prepared               # 程序启动时，检测已注册的model类，对每个类，触发

from .models import Img,Uploadimgs

# pre_save对象save前触发,压缩并保存图片
@receiver(pre_save, sender=Img)
def handle_image_compression(sender, instance, **kwargs):
    try:
        post_obj = Img.objects.get(pk=instance.pk)
        if post_obj.image != instance.image:
            instance.image = compress_image(instance.image)
    except Img.DoesNotExist:
        # the object does not exists, so compress the image
        if instance.image:
            instance.image = compress_image(instance.image)


# pre_delete对象delete前触发,删除图片
@receiver(pre_delete, sender=Img)
def delete(sender, instance, **kwargs):
    # Pass false so FileField doesn't save the model.
    instance.image.delete(False)

# 修改时,保存图片并删除旧图
@receiver(post_init, sender=Img)
def file_path(sender, instance, **kwargs):
    instance._current_image = instance.image

# post_save对象save后触发
@receiver(post_save, sender=Img)
def delete_old_image(sender, instance, **kwargs):
    if hasattr(instance, '_current_image'):
        if instance._current_image != instance.image:
            instance._current_image.delete(save=False)
# 压缩
def compress_image(image):
    # 打开
    out = BytesIO()
    print(image)
    if image == 'Project\muban\dig_close.png':
        compressed = 'dig_close.png'
        return compressed

    else:
        img = Image.open(image)
        if img.mode == 'RGBA':
            img = img.convert("RGB")  # 这个if与save对应（JPEG），当上传png会报错（cannot write mode RGBA as JPEG）
        width = img.width
        height = img.height
        rate = 1.0  # 压缩率

        # 根据图像大小设置压缩率
        if width >= 2000 or height >= 2000:
            rate = 0.2
        elif width >= 1000 or height >= 1000:
            rate = 0.4
        elif width >= 500 or height >= 500:
            rate = 1

        width = int(width * rate)  # 新的宽
        height = int(height * rate)  # 新的高
        print(width)
        img = img.resize((width, height), Image.ANTIALIAS)
        img.save(out, 'JPEG', quality=70)
        compressed = File(out, name=image.name)
        img.close()
        return compressed