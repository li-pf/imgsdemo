import json

from django import forms
from django.db import models
from datetime import datetime
from imgsdemo import settings
import xadmin
from django.utils.safestring import mark_safe
from .models import Uploadimgs,Img


class Uploadimgsadmin(object):
    list_display = ['name','imgs']

    def save_models(self):
        if self.new_obj:
            obj = self.new_obj
            files = self.request.FILES.getlist('imgs')
            print(files)
            print(obj.pk)
            try:
                img_url = Uploadimgs.objects.get(pk=obj.pk).imgs
                img_url = eval(img_url.name)
            except Exception as e:
                print(e)
                img_url = []
                obj.imgs = json.dumps(img_url, ensure_ascii=False)
                obj.save()

            if files:
                for f in files:
                    img_obj = Img()
                    img_obj.model = obj
                    img_obj.image = f
                    img_obj.save()
                    img_url.append(img_obj.image.name)

            str_json = json.dumps(img_url, ensure_ascii=False)
            print(str_json)
            obj.imgs = str_json
            obj.save()
        super().save_models()


class Imgadmin(object):
    list_display = ['model','creattime','upload_img']

    def upload_img(self, obj):
        try:
            img = mark_safe('<img src="%s" width="50px" />' % (obj.image.url,))
        except Exception as e:
            img = ''
        return img
    upload_img.short_description = '检查图片'
    upload_img.allow_tags = True

xadmin.site.register(Uploadimgs,Uploadimgsadmin)
xadmin.site.register(Img,Imgadmin)